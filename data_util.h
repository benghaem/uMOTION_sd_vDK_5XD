#ifndef _DATA_UTIL_H_
#define _DATA_UTIL_H_

#include <stdint.h>
#include "bmi160_drv/bmi160.h"


#define PACK_ACC    (0x1 << 0)
#define PACK_GYRO   (0x1 << 1)
#define PACK_MAG    (0x1 << 2)

#define PACK_START  (uint16_t)(0xE100)

#define BASE_PACK_SIZE   (4)
#define ACC_PACK_SIZE    (6)
#define GYRO_PACK_SIZE   (6) 
#define MAG_PACK_SIZE    (6)
#define MAX_PACK_SIZE    (BASE_PACK_SIZE + ACC_PACK_SIZE + GYRO_PACK_SIZE + MAG_PACK_SIZE) 

void pack_data(uint8_t flags, uint16_t * arr, uint16_t* time,
    struct bmi160_accel_t * samp_accelxyz, struct bmi160_gyro_t * samp_gyroxyz,
    struct bmi160_mag_t * samp_magxyz);

#endif /* _DATA_UTIL_H_ */
