/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*-----------------------------------------------------------------------*/
/* MMC/SDC (in SPI mode) control module  (C)ChaN, 2007                   */
/*-----------------------------------------------------------------------*/

#include <stdint.h>
#include <stdbool.h>

/*
 * By default disable both asserts and log for this module.
 * This must be done before DebugP.h is included.
 */
#ifndef DebugP_ASSERT_ENABLED
#define DebugP_ASSERT_ENABLED 0
#endif
#ifndef DebugP_LOG_ENABLED
#define DebugP_LOG_ENABLED 0
#endif
#include <ti/drivers/ports/DebugP.h>
#include <ti/drivers/ports/HwiP.h>
#include <ti/drivers/ports/ClockP.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/family/arm/m3/Hwi.h>


/* Debug drivers */
/* XDCtools Header files */
#include <xdc/std.h>
#include <xdc/runtime/System.h>

/* Normal Header */
#include <SDSPICC2650.h>
#include "Board.h"
/* Drivers for SPI / GPIO */
#include <ti/drivers/PIN.h>
#include <ti/drivers/SPI.h>

/* Board header */

/* Definitions for MMC/SDC command */
#define CMD0                        (0x40+0)    /* GO_IDLE_STATE */
#define CMD1                        (0x40+1)    /* SEND_OP_COND */
#define CMD8                        (0x40+8)    /* SEND_IF_COND */
#define CMD9                        (0x40+9)    /* SEND_CSD */
#define CMD10                       (0x40+10)   /* SEND_CID */
#define CMD12                       (0x40+12)   /* STOP_TRANSMISSION */
#define CMD16                       (0x40+16)   /* SET_BLOCKLEN */
#define CMD17                       (0x40+17)   /* READ_SINGLE_BLOCK */
#define CMD18                       (0x40+18)   /* READ_MULTIPLE_BLOCK */
#define CMD23                       (0x40+23)   /* SET_BLOCK_COUNT */
#define CMD24                       (0x40+24)   /* WRITE_BLOCK */
#define CMD25                       (0x40+25)   /* WRITE_MULTIPLE_BLOCK */
#define CMD41                       (0x40+41)   /* SEND_OP_COND (ACMD) */
#define CMD55                       (0x40+55)   /* APP_CMD */
#define CMD58                       (0x40+58)   /* READ_OCR */

#define SD_SECTOR_SIZE              512

#define START_BLOCK_TOKEN           0xFE
#define START_MULTIBLOCK_TOKEN      0xFC
#define STOP_MULTIBLOCK_TOKEN       0xFD

#define DRIVE_NOT_MOUNTED           ~0

/*
 * Array of SDSPI_Handles to determine the association of the FatFs drive number
 * with a SDSPI_Handle
 * _VOLUMES is defined in <ti/mw/fatfs/ffconf.h>
 */
static SDSPI_Handle sdspiHandles[_VOLUMES];

/* uS scaling to function timeouts */
static uint32_t       uSClockPeriod = 0;

/* Function prototypes */
static uint32_t       rcvr_datablock(SDSPICC2650_HWAttrs const *hwAttrs,
                                     uint8_t *buf, uint32_t btr);
void    releaseSPIBus(SDSPICC2650_HWAttrs const *hwAttrs);
uint8_t rxSPI(void);
static uint8_t        send_cmd(SDSPICC2650_HWAttrs const *hwAttrs, uint8_t cmd,
                               uint32_t arg);
static void           send_initial_clock_train(SDSPICC2650_HWAttrs const *hwAttrs);
void    takeSPIBus(SDSPICC2650_HWAttrs const *hwAttrs);
inline void    txSPI(uint8_t dat);
inline void 	txSPIBatch(uint8_t dat[], uint8_t size);
static uint8_t        wait_ready(SDSPICC2650_HWAttrs const *hwAttrs);
static bool           xmit_datablock(SDSPICC2650_HWAttrs const *hwAttrs,
                                     const uint8_t *buf, uint8_t token);

/* FatFs disk I/O functions */
DSTATUS SDSPICC2650_diskInitialize(BYTE drv);
DRESULT SDSPICC2650_diskIOctrl(BYTE drv, BYTE ctrl, void *buf);
DRESULT SDSPICC2650_diskRead(BYTE drv, BYTE *buf,
                             DWORD sector, UINT count);
DSTATUS SDSPICC2650_diskStatus(BYTE drv);
DRESULT SDSPICC2650_diskWrite(BYTE drv, const BYTE *buf,
                              DWORD sector, UINT count);

/* SDSPICC2650 functions */
void         SDSPICC2650_close(SDSPI_Handle handle);
int          SDSPICC2650_control(SDSPI_Handle handle, unsigned int cmd, void *arg);
void         SDSPICC2650_init(SDSPI_Handle handle);
SDSPI_Handle SDSPICC2650_open(SDSPI_Handle handle, unsigned char drv,
                              SDSPI_Params *params);

/* SDSPI function table for SDSPICC2650 implementation */
const SDSPI_FxnTable SDSPICC2650_fxnTable = {
    SDSPICC2650_init,
    SDSPICC2650_open,
    SDSPICC2650_close,
    SDSPICC2650_control
};

/* SPI Params global structure */
SPI_Params SPI_SD_params;


/* Pin driver state objects */
static PIN_State stateSPI_PIN_FULL;
static PIN_State stateSPI_PIN_CS;

/* Pin driver handle objects */
static PIN_Handle handleSPI_FULL;
static PIN_Handle handleSPI_CS;

static SPI_Handle SPI_Primary_Handle;

/*
 *  ======== rcvr_datablock ========
 *  Function to receive a block of data from the SDCard
 *
 *  btr count must be an even number
 */
static uint32_t rcvr_datablock(SDSPICC2650_HWAttrs const *hwAttrs,
                               uint8_t *buf, uint32_t btr)
{
    uint8_t   token;
    uint32_t  clockTimeout;
    uint32_t  clockStart;
    uint32_t  clockCurrent;

    /* Wait for data packet in timeout of 100 ms */
    clockStart = ClockP_getSystemTicks();
    clockTimeout = clockStart + (100 * 1000/uSClockPeriod);
    if (clockTimeout > clockStart) {
        clockStart = ~0;
    }
    do {
        token = rxSPI();
        clockCurrent = ClockP_getSystemTicks();
    } while ((token == 0xFF) && ((clockCurrent <= clockTimeout) ||
                                 (clockCurrent >= clockStart)));

    if (token != START_BLOCK_TOKEN) {
        /* If not valid data token, return error */
        return (0);
    }

    /* Receive the data block into buffer */
    do {
        *(buf++) = rxSPI();
    } while (--btr);

    /* Read the CRC, but discard it */
    rxSPI();
    rxSPI();

    /* Return with success */
    return (1);
}

/*
 *  ======== releaseSPIBus ========
 *  Function to release the SPI bus
 *
 */
void releaseSPIBus(SDSPICC2650_HWAttrs const *hwAttrs)
{
    /* Reset CS High */
    PIN_setOutputValue(handleSPI_CS, hwAttrs->csGPIOPin, 1);
}

/*
 *  ======== rxSPI ========
 *  Function to receive one byte onto the SPI bus. Polling (Blocked)
 *
 */
uint8_t rxSPI(void)
{

    SDSPIDataType   rcvdat;
    SPI_Transaction spiTrans;

    /* Set MOSI while rx */
    uint8_t dummy_data = 0xFF;

    spiTrans.count = 1;
    spiTrans.txBuf = &dummy_data;
    spiTrans.rxBuf = &rcvdat;

    //System_printf("SPI XFER (rxSPI) start\n");
    //System_flush();

    /* Do spi xfer */
    int ret = SPI_transfer(SPI_Primary_Handle, &spiTrans);
    if (!ret){
        System_printf("SPI XFER (rxSPI) failed\n");
        System_flush();
    }

    //System_printf("SPI XFER (rxSPI) done: %x\n",(uint8_t)rcvdat);
    //System_flush();

    return ((uint8_t)rcvdat);
}

/*
 *  ======== send_cmd ========
 *  Function that will transmit an command to the SDCard
 *
 *  @param  hwAttrs     Pointer to hardware attributes
 *
 *  @param  cmd         SD command
 *
 *  @param  arg         SD command argument
 */
static uint8_t send_cmd(SDSPICC2650_HWAttrs const *hwAttrs, uint8_t cmd, uint32_t arg)
{
    uint8_t n;
    uint8_t res;


    //System_printf("Sending CMD: %d\n", cmd);
	//System_flush();

    /* For all commands other than 0 we should wait for the card to ready */
	if (cmd != CMD0){
	    if (wait_ready(hwAttrs) != 0xFF) {

			System_abort("Error starting the SD card\n");
	    }
	}

    /* Send command packet */
	uint8_t packet[7] = {0};

	packet[0] = cmd;
	packet[6] = 0xFF;  //extra time for card to complete op
	packet[1]=(uint8_t)(arg >> 24);   /* Argument[31..24] */
    packet[2]=(uint8_t)(arg >> 16);   /* Argument[23..16] */
    packet[3]=(uint8_t)(arg >> 8);
    packet[4]=(uint8_t)arg;

    if (cmd == CMD0) {
        /* CRC for CMD0(0) */
        n = 0x95;
    }
    else if (cmd == CMD8) {
        /* CRC for CMD8(0x1AA) */
        n = 0x87;
    }
    else {
        /* Default CRC should be at least 0x01 */
        n = 0x01;
    }

    packet[5] = n;
    
    /* Future enhancement to add CRC support */
    txSPIBatch(packet, 6);


    /* Receive command response */
    if (cmd == CMD12) {
        /* Skip a stuff byte when stop reading */
        rxSPI();
    }

    /* Wait for a valid response in timeout; 10 attempts */
    n = 10;
    do {
        res = rxSPI();
    } while ((res & 0x80) && --n);

    //System_printf("Response: %x\n", res);
    //System_flush();

    /* Return with the response value */
    return (res);
}

/*
 *  ======== send_initial_clock_train ========
 *  Function to get the SDCard into SPI mode
 *
 *  @param  hwAttrs     Pointer to hardware attributes
 */
static void send_initial_clock_train(SDSPICC2650_HWAttrs const *hwAttrs)
{

    unsigned char   i;

    /* Pin configuration: Set CS high, MOSI high, MISO high, and clock */
    const PIN_Config aPinListSDInit[] = {
        hwAttrs->csGPIOPin | PIN_GPIO_OUTPUT_EN | PIN_GPIO_HIGH | PIN_PUSHPULL,
        hwAttrs->txGPIOPin | PIN_GPIO_OUTPUT_EN | PIN_GPIO_HIGH | PIN_PUSHPULL,
        hwAttrs->clkGPIOPin | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL,
        hwAttrs->rxGPIOPin | PIN_INPUT_EN | PIN_PULLUP,
        PIN_TERMINATE
    };

    // Get handle to this collection of pins
    handleSPI_FULL = PIN_open(&stateSPI_PIN_FULL, aPinListSDInit);
    if (!handleSPI_FULL) {
        // Handle allocation error
        System_printf("Failed to grab new pin handle for initial clock train\n");
        System_flush();

    }

    //TOGGLE the CLK pin 100 times
    //System_printf("Starting CLK toggle at approx 240kHz\n");
    //System_flush();

    /* We need to disable other operations to ensure a constant clock rate */
    unsigned int tkey = Task_disable();
    unsigned int hkey = HwiP_disable();

    for (i = 0; i < 100; i++) {
    	asm(" NOP");
    	asm(" NOP");
    	asm(" NOP");
        asm(" NOP");
        PIN_setOutputValue(handleSPI_FULL, hwAttrs->clkGPIOPin, 0);
    	asm(" NOP");
    	asm(" NOP");
    	asm(" NOP");
        asm(" NOP");
        PIN_setOutputValue(handleSPI_FULL, hwAttrs->clkGPIOPin, 1);
    }

    PIN_setOutputValue(handleSPI_FULL, hwAttrs->clkGPIOPin, 0);

    /* Restore other interrupts and tasks in reverse order */
    HwiP_restore(hkey);
    Task_restore(tkey);

    //System_printf("CLK toggle complete\n");
    //System_flush();

   // Revert to previous control configuration
    PIN_close(handleSPI_FULL);

    DebugP_log1("SDSPI:(%p) initialized SD card to SPI mode",
                hwAttrs->baseAddr);
}

/*
 *  ======== takeSPIBus ========
 *  Function to take the SPI bus
 *
 */
void takeSPIBus(SDSPICC2650_HWAttrs const *hwAttrs)
{

    /* Select the SD card. Set CS low */
    PIN_setOutputValue(handleSPI_CS, hwAttrs->csGPIOPin, 0);

}

/*
 *  ======== txSPI ========
 *  Function to transmit one byte onto the SPI bus. Polling (Blocked)
 *
 *  @param  dat         Data to be sent onto the SPI bus
 */
inline void txSPI(uint8_t dat)
{
    SDSPIDataType   rcvdat;

    SPI_Transaction spiTrans;

    spiTrans.count = 1;
    spiTrans.txBuf = &dat;
    spiTrans.rxBuf = &rcvdat;

//    System_printf("SPI XFER (txSPI) starting ( %02x )\n", dat);
//    System_flush();

    /* Start SPI xfer */
    int ret = SPI_transfer(SPI_Primary_Handle, &spiTrans);
    if (!ret){
//        System_printf("SPI XFER (txSPI) failed\n");
//        System_flush();
    }

//    System_printf("SPI XFER (txSPI) complete %02x\n", dat);
//    System_flush();
}

inline void txSPIBatch(uint8_t dat[], uint8_t size){
    SDSPIDataType   rcvdat;

    SPI_Transaction spiTrans;

    spiTrans.count = size;
    spiTrans.txBuf = dat;
    spiTrans.rxBuf = &rcvdat;

    //System_printf("SPI XFER (txSPIBatch) starting. Size (%d)", size);
    //System_flush();

    /* Start SPI xfer */
    int ret = SPI_transfer(SPI_Primary_Handle, &spiTrans);
    if (!ret){
        System_printf("SPI XFER (txSPIBatch) failed\n");
        System_flush();
    }

//    System_printf("SPI XFER (txSPIBatch) complete \n");
//    System_flush();
}

/*
 *  ======== wait_ready ========
 *  Function to check if the SDCard is busy
 *
 *  This function queries the SDCard to see if it is in a busy state or ready
 *  state
 *
 *  @param  hwAttrs     Pointer to hardware attributes
 */
static uint8_t wait_ready(SDSPICC2650_HWAttrs const *hwAttrs)
{
    uint8_t   res;
    //uint32_t  clockTimeout;
    //uint32_t  clockStart;
    //uint32_t  clockCurrent;

    /* Wait for data packet in timeout of 500 ms */
    //clockStart = ClockP_getSystemTicks();
    //clockTimeout = clockStart + (5000 * 1000/ClockP_getSystemTickPeriod());
    //if (clockTimeout > clockStart) {
    //    clockStart = ~0;
    //}
    rxSPI();
    do {
        res = rxSPI();
      //  clockCurrent = ClockP_getSystemTicks();
    } while ((res != 0xFF) /*&& ((clockCurrent <= clockTimeout) ||
                               (clockCurrent >= clockStart))*/);

    return (res);
}

/* _READONLY is defined in <ti/mw/fatfs/diskio.h> */
#if _READONLY == 0
/*
 *  ======== xmit_datablock ========
 *  Function to transmit a block of data to the SDCard
 *
 *  @param  hwAttrs     Pointer to hardware attributes
 *
 *  @param  params      SDSPICC2650 hardware attributes
 *
 *  @param  buf         pointer to const data buffer
 *
 *  @param  token       command token to be sent to the SD card prior to
 *                      sending the data block. The available tokens are:
 *                      START_BLOCK_TOKEN
 *                      START_MULTIBLOCK_TOKEN
 *                      STOP_MULTIBLOCK_TOKEN
 */
static bool xmit_datablock(SDSPICC2650_HWAttrs const *hwAttrs,
                           const uint8_t *buf, uint8_t token)
{
    uint8_t resp;
    uint8_t wc;

    if (wait_ready(hwAttrs) != 0xFF) {
        /* Return with error */
        return (false);
    }

    /* Xmit data token */
    txSPI(token);

    /* Send data only when token != STOP_MULTIBLOCK_TOKEN */
    if (token != STOP_MULTIBLOCK_TOKEN) {
        /* Is data token */
        wc = 0;
        /* Transferring 512 byte blocks using a 8 bit counter */
        do {
            /* Xmit the SD_SECTOR_SIZE byte data block */
            txSPI(*buf++);
            txSPI(*buf++);
        } while (--wc);

        /* Future enhancement to add CRC support */
        txSPI(0xFF);
        txSPI(0xFF);

        /* Reveive data response */
        resp = rxSPI();

        /* If not accepted, return error */
        if ((resp & 0x1F) != 0x05) {
            return (false);
        }
    }

    /* Return with success */
    return (true);
}
#endif /* _READONLY */

/*
 *  ======== SDSPICC2650_close ========
 *  Function to unmount the FatFs filesystem and unregister the SDSPICC2650
 *  disk I/O functions from SYS/BIOS' FatFS module.
 *
 *  @param  handle      SDSPI_Handle returned by SDSPI_open()
 */
void SDSPICC2650_close(SDSPI_Handle handle)
{
    uintptr_t                    key;
    DRESULT                      dresult;
    FRESULT                      fresult;
    SDSPICC2650_Object          *object = handle->object;
    TCHAR                        path[3];

    path[0] = '0' + object->driveNumber;
    path[1] = ':';
    path[2] = '\0';


    /* Unmount the FatFs drive */
    fresult = f_mount(NULL, path, 0);
    if (fresult != FR_OK) {
        DebugP_log2("SDSPI:(%p) Could not unmount FatFs volume @ drive number %d",
                    hwAttrs->baseAddr, object->driveNumber);
    }

    /* Unregister the disk_*() functions */
    dresult = disk_unregister(object->driveNumber);
    if (dresult != RES_OK) {
        DebugP_log2("SDSPI:(%p) Error unregistering disk functions @ drive number %d",
                    hwAttrs->baseAddr, object->driveNumber);
    }

    PIN_close(handleSPI_CS);

    /* close the handle so we can use it for other things */
    SPI_close(SPI_Primary_Handle);

    DebugP_log1("SDSPI:(%p) closed", hwAttrs->baseAddr);

    key = HwiP_disable();
    object->driveNumber = DRIVE_NOT_MOUNTED;
    HwiP_restore(key);
}

/*
 *  ======== SDSPICC2650_control ========
 *  @pre    Function assumes that the handle is not NULL
 */
int SDSPICC2650_control(SDSPI_Handle handle, unsigned int cmd, void *arg)
{
    /* No implementation yet */
    return (SDSPI_STATUS_UNDEFINEDCMD);
}

/*
 *  ======== SDSPICC2650_diskInitialize ========
 *  Function to initialize the SD Card.  This function is called by the FatFs
 *  module and must not be called by the application!
 *
 *  @param  drv         Drive Number
 */
DSTATUS SDSPICC2650_diskInitialize(BYTE drv)
{
    uint8_t                    n;
    uint8_t                    ocr[4];
    SDSPICC2650_CardType       cardType;
    ClockP_FreqHz              freq;
    uint32_t                   clockTimeout;
    uint32_t                   clockStart;
    uint32_t                   clockCurrent;
    SDSPICC2650_Object        *object = sdspiHandles[drv]->object;
    SDSPICC2650_HWAttrs const *hwAttrs = sdspiHandles[drv]->hwAttrs;

    /* No card in the socket */
    if (object->diskState & STA_NODISK) {
        DebugP_log1("SDSPI:(%p) disk initialization failed: No disk",
                    hwAttrs->baseAddr);

        return (object->diskState);
    }

    System_printf("Attempting to initialize the SD card\n");
	System_flush();

    /* Initialize the SD Card for SPI mode */
    send_initial_clock_train(hwAttrs);


    //System_printf("SD card should now be in SPI mode\n");
	//System_flush();


     /*Grab SPI*/
    SPI_Primary_Handle = SPI_open(Board_SPI0, &SPI_SD_params);
    if (!SPI_Primary_Handle) {
        System_printf("SPI did not open\n");
        System_flush();
    }


    const PIN_Config SD_CS_GPIO[] = {
        hwAttrs->csGPIOPin | PIN_GPIO_OUTPUT_EN | PIN_GPIO_HIGH | PIN_PUSHPULL,
        PIN_TERMINATE
    };

      // Get handle to this collection of pins
    handleSPI_CS = PIN_open(&stateSPI_PIN_CS, SD_CS_GPIO);
    if (!handleSPI_CS) {
        // Handle allocation error
        System_printf("Failed to grab new pin handle for CS\n");
        System_flush();

    }

    /* Select the SD Card's chip select */
    takeSPIBus(hwAttrs);
    cardType = SDSPICC2650_NOCARD;


    //System_printf("Send CMD0 \n");
	//System_flush();

    /* Send the CMD0 to put the SD Card in "Idle" state */

	uint8_t ret = 0;
	do{
	    ret = send_cmd(hwAttrs, CMD0, 0);
	} while (ret != 0x01);

    if (ret == 1) {


    	//System_printf("Got good response from CMD0 continuing \n");
    	//System_flush();


        /*
         * Determine what SD Card version we are dealing with
         * Depending on which SD Card version, we need to send different SD
         * commands to the SD Card, which will have different response fields.
         */
        if (send_cmd(hwAttrs, CMD8, 0x1AA) == 1) {
            /* SDC Ver2+ */
            for (n = 0; n < 4; n++) {
                ocr[n] = rxSPI();
            }

            /*
             * Ensure that the card's voltage range is valid
             * The card can work at vdd range of 2.7-3.6V
             */

            //System_printf("Check card voltage range\n");
    		//System_flush();

            if ((ocr[2] == 0x01) && (ocr[3] == 0xAA)) {
                /* Wait for data packet in timeout of 1s */
                clockStart = ClockP_getSystemTicks();
                clockTimeout = clockStart + (1000 * 1000/uSClockPeriod);
                if (clockTimeout > clockStart) {
                    clockStart = ~0;
                }
                do {
                    /* ACMD41 with HCS bit */
                    if (send_cmd(hwAttrs, CMD55, 0) <= 1 &&
                        send_cmd(hwAttrs, CMD41, 1UL << 30) == 0) {
                        clockTimeout = 0;
                        break;
                    }
                    clockCurrent = ClockP_getSystemTicks();
                } while ((clockCurrent <= clockTimeout) ||
                         (clockCurrent >= clockStart));

                /*
                 * Check CCS bit to determine which type of capacity we are
                 * dealing with
                 */
                if ((!clockTimeout) && send_cmd(hwAttrs, CMD58, 0) == 0) {
                    for (n = 0; n < 4; n++) {
                        ocr[n] = rxSPI();
                    }
                    cardType = (ocr[0] & 0x40) ? SDSPICC2650_SDHC : SDSPICC2650_SDSC;
                }
            }
        }

        /* SDC Ver1 or MMC */
        else {

        	System_printf("Got bad response from CMD0 continuing \n");
        	System_flush();
            /*
             * The card version is not SDC V2+ so check if we are dealing with a
             * SDC or MMC card
             */
            if ((send_cmd(hwAttrs, CMD55, 0) <= 1 &&
                 send_cmd(hwAttrs, CMD41, 0) <= 1)) {
                cardType = SDSPICC2650_SDSC;
            }
            else {
                cardType = SDSPICC2650_MMC;
            }

            /* Wait for data packet in timeout of 1s */
            clockStart = ClockP_getSystemTicks();
            clockTimeout = clockStart + (1000 * 1000/uSClockPeriod);
            if (clockTimeout > clockStart) {
                clockStart = ~0;
            }
            do {
                if (cardType == SDSPICC2650_SDSC) {
                    /* ACMD41 */
                    if (send_cmd(hwAttrs, CMD55, 0) <= 1 &&
                        send_cmd(hwAttrs, CMD41, 0) == 0) {
                        clockTimeout = 0;
                        break;
                    }
                }
                else {
                    /* CMD1 */
                    if (send_cmd(hwAttrs, CMD1, 0) == 0) {
                        clockTimeout = 0;
                        break;
                    }
                }
                clockCurrent = ClockP_getSystemTicks();
            } while ((clockCurrent <= clockTimeout) ||
                     (clockCurrent >= clockStart));

            /* Select R/W block length */
            if ((clockTimeout) || send_cmd(hwAttrs, CMD16, SD_SECTOR_SIZE) != 0) {
                cardType = SDSPICC2650_NOCARD;
            }
        }
    }

    object->cardType = cardType;

    /* Deselect the SD Card's chip select */
    releaseSPIBus(hwAttrs);



    /* Idle (Release DO) */
   // rxSPI();

    /* Check to see if a card type was determined */
    if (cardType != SDSPICC2650_NOCARD) {

    	System_printf("Card type found to be %d\n", cardType);
    	System_flush();
        /* Reconfigure the SPI bus at the new frequency rate */
        ClockP_getCpuFreq(&freq);

        SPI_SD_params.bitRate = object->bitRate;

        DebugP_log3("SDSPI:(%p) CPU freq: %d; Reconfiguring SDSPI freq to %d",
                    hwAttrs->baseAddr, freq.lo, object->bitRate);

        /* Initialization succeeded */
        object->diskState &= ~STA_NOINIT;
    }
    else {
        DebugP_log1("SDSPI:(%p) disk initialization failed",
                    hwAttrs->baseAddr);
        System_printf("Card failed to respond to CMD0\n");
        System_flush();
    }

    return (object->diskState);
}

/*
 *  ======== SDSPICC2650_diskIOctrl ========
 *  Function to perform specified disk operations. This function is called by the
 *  FatFs module and must not be called by the application!
 *
 *  @param  drv         Drive Number
 *
 *  @param  ctrl        Control code
 *
 *  @param  buf         Buffer to send/receive control data
 */
DRESULT SDSPICC2650_diskIOctrl(BYTE drv, BYTE ctrl, void *buf)
{
    DRESULT                    res = RES_ERROR;
    uint8_t                    n;
    uint8_t                    csd[16];
    WORD                       csize;
    SDSPICC2650_Object        *object = sdspiHandles[drv]->object;
    SDSPICC2650_HWAttrs const *hwAttrs = sdspiHandles[drv]->hwAttrs;

    if (object->diskState & STA_NOINIT) {
        DebugP_log1("SDSPI:(%p) disk IO control: disk not initialized",
                    hwAttrs->baseAddr);

        return (RES_NOTRDY);
    }

    /* Select the SD Card's chip select */
    takeSPIBus(hwAttrs);


    switch (ctrl) {
        case GET_SECTOR_COUNT:
            /* Get number of sectors on the disk (uint32_t) */
            if ((send_cmd(hwAttrs, CMD9, 0) == 0) &&
                 rcvr_datablock(hwAttrs, csd, 16)) {

                /* SDC ver 2.00 */
                if ((csd[0] >> 6) == 1) {
                    csize = csd[9] + ((WORD)csd[8] << 8) + 1;
                    *(uint32_t*)buf = (uint32_t)csize << 10;
                }
                /* MMC or SDC ver 1.XX */
                else {
                    n =  (csd[5] & 15) +
                        ((csd[10] & 128) >> 7) +
                        ((csd[9] & 3) << 1) + 2;

                    csize =        (csd[8] >> 6) +
                             ((WORD)csd[7] << 2) +
                            ((WORD)(csd[6] & 3) << 10) + 1;

                    *(uint32_t*)buf = (uint32_t)csize << (n - 9);
                }
                DebugP_log2("SDSPI:(%p) disk IO control: sector count: %d",
                            hwAttrs->baseAddr, *(uint32_t*)buf);
                res = RES_OK;
            }
            break;

        case GET_SECTOR_SIZE:
            /* Get sectors on the disk (WORD) */
            *(WORD*)buf = SD_SECTOR_SIZE;
            DebugP_log2("SDSPI:(%p) disk IO control: sector size: %d",
                        hwAttrs->baseAddr, *(WORD*)buf);
            res = RES_OK;
            break;

        case CTRL_SYNC:
            /* Make sure that data has been written */
            if (wait_ready(hwAttrs) == 0xFF) {
                DebugP_log1("SDSPI:(%p) disk IO control: control sync: ready",
                            hwAttrs->baseAddr);
                res = RES_OK;
            }
            else {
                DebugP_log1("SDSPI:(%p) disk IO control: control sync: not ready",
                            hwAttrs->baseAddr);
                res = RES_NOTRDY;
            }
            break;

        default:
            DebugP_log1("SDSPI:(%p) disk IO control: parameter error",
                        hwAttrs->baseAddr);
            res = RES_PARERR;
            break;
    }

    /* Deselect the SD Card's chip select */
    releaseSPIBus(hwAttrs);

    /* Idle (Release DO) */
    //rxSPI();

    return (res);
}

/*
 *  ======== SDSPICC2650_diskRead ========
 *  Function to perform a disk read from the SDCard. This function is called by
 *  the FatFs module and must not be called by the application!
 *
 *  @param  drv         Drive Number
 *
 *  @param  buf         Pointer to a buffer on which to store data
 *
 *  @param  sector      Starting sector number (LBA)
 *
 *  @param  count       Sector count (1...255)
 */
DRESULT SDSPICC2650_diskRead(BYTE drv, BYTE *buf,
                             DWORD sector, UINT count)
{
    SDSPICC2650_Object        *object = sdspiHandles[drv]->object;
    SDSPICC2650_HWAttrs const *hwAttrs = sdspiHandles[drv]->hwAttrs;

    if (!count) {
        DebugP_log1("SDSPI:(%p) disk read: 0 sectors to read",
                    hwAttrs->baseAddr);

        return (RES_PARERR);
    }

    if (object->diskState & STA_NOINIT) {
        DebugP_log1("SDSPI:(%p) disk read: disk not initialized",
                    hwAttrs->baseAddr);

        return (RES_NOTRDY);
    }

    /*
     * On a SDSC card, the sector address is a byte address on the SD Card
     * On a SDHC card, the sector address is address by sector blocks
     */
    if (object->cardType != SDSPICC2650_SDHC) {
        /* Convert to byte address */
        sector *= SD_SECTOR_SIZE;
    }

    /* Select the SD Card's chip select */
    takeSPIBus(hwAttrs);

    /* Single block read */
    if (count == 1) {
        if ((send_cmd(hwAttrs, CMD17, sector) == 0) &&
             rcvr_datablock(hwAttrs, buf, SD_SECTOR_SIZE)) {
            count = 0;
        }
    }
    /* Multiple block read */
    else {
        if (send_cmd(hwAttrs, CMD18, sector) == 0) {
            do {
                if (!rcvr_datablock(hwAttrs, buf, SD_SECTOR_SIZE)) {
                    break;
                }
                buf += SD_SECTOR_SIZE;
            } while (--count);

            /* STOP_TRANSMISSION */
            send_cmd(hwAttrs, CMD12, 0);
        }
    }

    /* Deselect the SD Card's chip select */
    releaseSPIBus(hwAttrs);

    /* Idle (Release DO) */
    rxSPI();

    return (count ? RES_ERROR : RES_OK);
}

/*
 *  ======== SDSPICC2650_diskStatus ========
 *  Function to return the current disk status. This function is called by
 *  the FatFs module and must not be called by the application!
 *
 *  @param(drv)         Drive Number
 */
DSTATUS SDSPICC2650_diskStatus(BYTE drv)
{
    /* Get the pointer to the object */
    SDSPICC2650_Object  *object = sdspiHandles[drv]->object;

    DebugP_log2("SDSPI:(%p) disk status: diskState: %d",
                ((SDSPICC2650_HWAttrs const *)(sdspiHandles[drv]->hwAttrs))->baseAddr,
                object->diskState);

    return (object->diskState);
}

#if _READONLY == 0
/*
 *  ======== SDSPICC2650_diskWrite ========
 *  Function to perform a disk write from the SDCard. This function is called by
 *  the FatFs module and must not be called by the application!
 *
 *  @param  drv         Drive Number
 *
 *  @param  buf         Pointer to a buffer from which to read data
 *
 *  @param  sector      Starting sector number (LBA)
 *
 *  @param  count       Sector count (1...255)
 */
DRESULT SDSPICC2650_diskWrite(BYTE drv, const BYTE *buf,
                            DWORD sector, UINT count)
{
    SDSPICC2650_Object        *object = sdspiHandles[drv]->object;
    SDSPICC2650_HWAttrs const *hwAttrs = sdspiHandles[drv]->hwAttrs;

    if (!count) {
        DebugP_log1("SDSPI:(%p) disk write: 0 sectors to write",
                    hwAttrs->baseAddr);

        return (RES_PARERR);
    }
    if (object->diskState & STA_NOINIT) {
        DebugP_log1("SDSPI:(%p) disk write: disk not initialized",
                    hwAttrs->baseAddr);

        return (RES_NOTRDY);
    }
    if (object->diskState & STA_PROTECT) {
        DebugP_log1("SDSPI:(%p) disk write: disk protected",
                    hwAttrs->baseAddr);

        return (RES_WRPRT);
    }

    /*
     * On a SDSC card, the sector address is a byte address on the SD Card
     * On a SDHC card, the sector address is address by sector blocks
     */
    if (object->cardType != SDSPICC2650_SDHC) {
        /* Convert to byte address if needed */
        sector *= SD_SECTOR_SIZE;
    }

    /* Select the SD Card's chip select */
    takeSPIBus(hwAttrs);

    /* Single block write */
    if (count == 1) {
        if ((send_cmd(hwAttrs, CMD24, sector) == 0) &&
             xmit_datablock(hwAttrs, buf, START_BLOCK_TOKEN)) {
            count = 0;
        }
    }
    /* Multiple block write */
    else {
        if ((object->cardType == SDSPICC2650_SDSC) ||
            (object->cardType == SDSPICC2650_SDHC)) {
            send_cmd(hwAttrs, CMD55, 0);
            send_cmd(hwAttrs, CMD23, count);    /* ACMD23 */
        }
        /* WRITE_MULTIPLE_BLOCK */
        if (send_cmd(hwAttrs, CMD25, sector) == 0) {
            do {
                if (!xmit_datablock(hwAttrs, buf, START_MULTIBLOCK_TOKEN)) {
                    break;
                }
                buf += SD_SECTOR_SIZE;
            } while (--count);

            /* STOP_TRAN token */
            if (!xmit_datablock(hwAttrs, 0, STOP_MULTIBLOCK_TOKEN)) {
                count = 1;
            }
        }
    }

    /* Deselect the SD Card's chip select */
    releaseSPIBus(hwAttrs);

    /* Idle (Release DO) */
    //rxSPI();

    return (count ? RES_ERROR : RES_OK);
}
#endif /* _READONLY */

/*
 *  ======== SDSPICC2650_init ========
 *  Function to initialize SDSPI module
 */
void SDSPICC2650_init(SDSPI_Handle handle)
{
    SDSPICC2650_Object       *object = handle->object;

    /* Mark the object as available */
    object->driveNumber = DRIVE_NOT_MOUNTED;
    object->diskState = STA_NOINIT;
    object->cardType = SDSPICC2650_NOCARD;
}

/*
 *  ======== SDSPICC2650_open ========
 *  Function to mount the FatFs filesystem and register the SDSPICC2650 disk
 *  I/O functions with SYS/BIOS' FatFS module.
 *
 *  This function also configures some basic GPIO settings needed for the
 *  software chip select with the SDCard.
 *
 *  @param  handle      SDSPI handle
 *  @param  drv         Drive Number
 *  @param  params      SDSPI parameters
 */
SDSPI_Handle SDSPICC2650_open(SDSPI_Handle handle,
                              unsigned char drv,
                              SDSPI_Params *params)
{
    DRESULT                    dresult;
    FRESULT                    fresult;
    uintptr_t                  key;
    SDSPICC2650_Object        *object = handle->object;
    //SDSPICC2650_HWAttrs const *hwAttrs = handle->hwAttrs;
    TCHAR                      path[3];

    /* Determine if the device was already opened */
    key = Hwi_disable();
    if (object->driveNumber != DRIVE_NOT_MOUNTED) {
        Hwi_restore(key);
        return (NULL);
    }
    /* Mark as being used */
    object->driveNumber = drv;
    Hwi_restore(key);

    /* Determine time scaling for timeouts */
    uSClockPeriod = ClockP_getSystemTickPeriod();
    DebugP_assert(uSClockPeriod != 0);

    /* Store desired bitRate */
    object->bitRate = params->bitRate;

    //Create params for initialization and params for normal usage
    //Set bitrate to 5000000
    SPI_Params_init(&SPI_SD_params);
    SPI_SD_params.bitRate = 5000000;

    DebugP_log1("SDSPI:(%p) SDSPI freq to 5MHz", hwAttrs->baseAddr);

    /* MAP_SPIEnable(hwAttrs->baseAddr); */

    /* Register the new disk_*() functions */
    dresult = disk_register(object->driveNumber,
                            SDSPICC2650_diskInitialize,
                            SDSPICC2650_diskStatus,
                            SDSPICC2650_diskRead,
                            SDSPICC2650_diskWrite,
                            SDSPICC2650_diskIOctrl);

    /* Check for drive errors */
    if (dresult != RES_OK) {
        DebugP_log1("SDSPI:(%p) disk functions not registered",
                    hwAttrs->baseAddr);

        SDSPICC2650_close(handle);
        return (NULL);
    }

    /*
     * Register the filesystem with FatFs. This operation does not access the
     * SDCard yet.
     */
    path[0] = '0' + object->driveNumber;
    path[1] = ':';
    path[2] = '\0';

    fresult = f_mount(&(object->filesystem), path, 0);
    if (fresult != FR_OK) {
        DebugP_log2("SDSPI:(%p) drive %d not mounted",
                    hwAttrs->baseAddr, object->driveNumber);

        SDSPICC2650_close(handle);
        return (NULL);
    }

    /* Store the new SDSPI handle for this FatFs drive number */
    sdspiHandles[drv] = handle;

    DebugP_log1("SDSPI:(%p) opened", hwAttrs->baseAddr);

    return (handle);
}
