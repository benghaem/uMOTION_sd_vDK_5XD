#include <time_util.h>

uint32_t get_tick(){
    static uint32_t tick = 0;
    tick++;
    return tick;
}
