################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CFG_SRCS += \
../uMOTION.cfg 

CMD_SRCS += \
../cc26x0f128.cmd 

C_SRCS += \
../BMI_I2C_Layer.c \
../CC2650DK_5XD.c \
../SDSPICC2650.c \
../ccfg.c \
../data_util.c \
../time_util.c \
../uMOTION.c 

GEN_CMDS += \
./configPkg/linker.cmd 

GEN_FILES += \
./configPkg/linker.cmd \
./configPkg/compiler.opt 

GEN_MISC_DIRS += \
./configPkg/ 

C_DEPS += \
./BMI_I2C_Layer.d \
./CC2650DK_5XD.d \
./SDSPICC2650.d \
./ccfg.d \
./data_util.d \
./time_util.d \
./uMOTION.d 

GEN_OPTS += \
./configPkg/compiler.opt 

OBJS += \
./BMI_I2C_Layer.obj \
./CC2650DK_5XD.obj \
./SDSPICC2650.obj \
./ccfg.obj \
./data_util.obj \
./time_util.obj \
./uMOTION.obj 

GEN_MISC_DIRS__QUOTED += \
"configPkg\" 

OBJS__QUOTED += \
"BMI_I2C_Layer.obj" \
"CC2650DK_5XD.obj" \
"SDSPICC2650.obj" \
"ccfg.obj" \
"data_util.obj" \
"time_util.obj" \
"uMOTION.obj" 

C_DEPS__QUOTED += \
"BMI_I2C_Layer.d" \
"CC2650DK_5XD.d" \
"SDSPICC2650.d" \
"ccfg.d" \
"data_util.d" \
"time_util.d" \
"uMOTION.d" 

GEN_FILES__QUOTED += \
"configPkg\linker.cmd" \
"configPkg\compiler.opt" 

C_SRCS__QUOTED += \
"../BMI_I2C_Layer.c" \
"../CC2650DK_5XD.c" \
"../SDSPICC2650.c" \
"../ccfg.c" \
"../data_util.c" \
"../time_util.c" \
"../uMOTION.c" 


