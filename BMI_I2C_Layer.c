/*
 * BMI_I2C_Layer.c
 *
 *  Created on: Dec 1, 2016
 *      Author: Ben
 */


#include "BMI_I2C_Layer.h"
#include <ti/drivers/I2C.h>
#include <xdc/runtime/System.h>
#include "Board.h"
#include <xdc/runtime/Diags.h>
#include <xdc/runtime/Log.h>

s8 I2C_BMI_bus_write(u8 device_addr, u8 register_addr,
								u8* data, u8 length){

	//because BMI only allows one data byte written after
	//the register addr then we know that the total size will always be
	//at maximum 2 long thus

	u8 complete_write_data[2] = {0};
	u8 complete_length = length + 1;

	//merge the register data and the data to be written
	complete_write_data[0] = register_addr;
	complete_write_data[1] = data[0];

    I2C_Handle      handle;
    I2C_Params      params;
	I2C_Transaction i2cTransaction;


	I2C_Params_init(&params);
	params.transferMode  = I2C_MODE_BLOCKING;
	params.bitRate = I2C_400kHz;
	handle = I2C_open(Board_I2C0, &params);
	if (!handle) {
	    System_printf("I2C did not open\n");
	    System_flush();
	    return ERROR;
	}

	i2cTransaction.writeBuf = complete_write_data;
	i2cTransaction.writeCount = complete_length;
	i2cTransaction.readCount = 0;
	i2cTransaction.slaveAddress = device_addr;


	bool ret;
	ret = I2C_transfer(handle, &i2cTransaction);
	if (!ret) {
	    Log_print1(Diags_USER1, "BMI: I2C Transaction Failure", 0);
	    I2C_close(handle);
		return ERROR;
    }

	 I2C_close(handle);
	 return SUCCESS;

}

s8 I2C_BMI_bus_read(u8 device_addr, u8 register_addr,
								u8* data, u8 length){

	 I2C_Handle      handle;
	 I2C_Params      params;
	 I2C_Transaction i2cTransaction;

	 I2C_Params_init(&params);
	 params.transferMode  = I2C_MODE_BLOCKING;
	 params.bitRate = I2C_400kHz;
	 handle = I2C_open(Board_I2C0, &params);
	 if (!handle) {
	     System_printf("I2C did not open\n");
	     System_flush();
	     return ERROR;
	 }

	 u8 regBuf[1] = { register_addr };

	 i2cTransaction.writeBuf = regBuf;
	 i2cTransaction.writeCount = 1;
	 i2cTransaction.readBuf = data;
	 i2cTransaction.readCount = length;
	 i2cTransaction.slaveAddress = device_addr;

	 bool ret;
	 ret = I2C_transfer(handle, &i2cTransaction);
	 if (!ret) {
	     Log_print1(Diags_USER1, "BMI: I2C Transaction Failure", 0);
		 I2C_close(handle);
		 return ERROR;
     }

	 I2C_close(handle);
	 return SUCCESS;
}

