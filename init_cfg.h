#include <stdint.h>

#ifndef INIT_CFG_DEFAULTS_H
#define INIT_CFG_DEFAULTS_H

/* Defaults for the user provided init.cfg file */
#define ICFG_ENABLE_ACC                 (1)
#define ICFG_ENABLE_GYRO                (1)

#define ICFG_SAMPLING_PERIOD            (1666) /*60 hz*/
#define ICFG_SAMPLE_GROUPS              (1800) /*10 minutes*/

#define ICFG_GYRO_RANGE                 (BMI160_GYRO_RANGE_2000_DEG_SEC) /*2000 deg/sec*/
#define ICFG_ACC_RANGE                  (BMI160_ACCEL_RANGE_16G)


/* Max and Min values */

#define ICFG_SAMPLING_PERIOD_MIN        (1666)
#define ICFG_SAMPLING_PERIOD_MAX        (1666)
#define ICFG_SAMPLE_GROUPS_MIN           (1)

struct ICFG_Config{
    uint8_t enable_acc;
    uint8_t enable_gyro;
    int samp_period;
    int samp_group_count;
    uint8_t gyro_range;
    uint8_t acc_range;
};

#endif
