/*
 * fs_util.h
 *
 *  Created on: Mar 10, 2017
 *      Author: bg5ng
 */

#ifndef FS_UTIL_H_
#define FS_UTIL_H_

/* Get the id of the last stored data file */
int last_data_file_id(void);

#endif /* FS_UTIL_H_ */
