#include "data_util.h"
/* 
 * TIME, ACC, GYRO, MAG
 */
void pack_data(uint8_t flags, uint16_t * arr, uint16_t* time, 
    struct bmi160_accel_t * samp_accelxyz, struct bmi160_gyro_t * samp_gyroxyz,
    struct bmi160_mag_t * samp_magxyz)
{
    int i = 0;
    arr[i++] = PACK_START | flags;
    arr[i++] = (*time);
    if (flags & PACK_ACC){
        arr[i++] = samp_accelxyz->x;
        arr[i++] = samp_accelxyz->y;
        arr[i++] = samp_accelxyz->z;
    }
    if (flags & PACK_GYRO){
        arr[i++] = samp_gyroxyz->x;
        arr[i++] = samp_gyroxyz->y;
        arr[i++] = samp_gyroxyz->z;
    }
    if (flags & PACK_MAG){
        /* Currently this is non-functional */
        arr[i++] = 0x0000; 
        arr[i++] = 0x0000; 
        arr[i++] = 0x0000; 
    } 

    return;
}
