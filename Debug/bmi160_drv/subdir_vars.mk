################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../bmi160_drv/bmi160.c \
../bmi160_drv/bmi160_lis3ml.c \
../bmi160_drv/bmi160_support.c 

C_DEPS += \
./bmi160_drv/bmi160.d \
./bmi160_drv/bmi160_lis3ml.d \
./bmi160_drv/bmi160_support.d 

OBJS += \
./bmi160_drv/bmi160.obj \
./bmi160_drv/bmi160_lis3ml.obj \
./bmi160_drv/bmi160_support.obj 

OBJS__QUOTED += \
"bmi160_drv\bmi160.obj" \
"bmi160_drv\bmi160_lis3ml.obj" \
"bmi160_drv\bmi160_support.obj" 

C_DEPS__QUOTED += \
"bmi160_drv\bmi160.d" \
"bmi160_drv\bmi160_lis3ml.d" \
"bmi160_drv\bmi160_support.d" 

C_SRCS__QUOTED += \
"../bmi160_drv/bmi160.c" \
"../bmi160_drv/bmi160_lis3ml.c" \
"../bmi160_drv/bmi160_support.c" 


