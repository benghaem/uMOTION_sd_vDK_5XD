/*
 * bmi60_lis3mdl.h
 *
 *  Created on: Dec 2, 2016
 *      Author: Ben
 */

#ifndef BMI160_DRV_BMI160_LIS3MDL_H_
#define BMI160_DRV_BMI160_LIS3MDL_H_

#include "../bmi160_drv/bmi160.h"

#define LIS3MDL_I2C_ADDR            (0x1C)
#define LIS3MDL_WHOAMI              (0x3D)

/* LIS3MDL WHOAMI Reg */
#define LIS3MDL_WHOAMI_REG          (0x0F)

/* LIS3MDL CTRL Regs */
#define LIS3MDL_CTRL_REG1           (0x20)
#define LIS3MDL_CTRL_REG2           (0x21)
#define LIS3MDL_CTRL_REG3           (0x22)
#define LIS3MDL_CTRL_REG4           (0x23)
#define LIS3MDL_CTRL_REG5           (0x24)

/* LIS3MDL OUT Regs */
#define LIS3MDL_STATUS              (0x27)
/* Mag values */
#define LIS3MDL_OUT_XL              (0x28)
#define LIS3MDL_OUT_XH              (0x29)
#define LIS3MDL_OUT_YL              (0x2A)
#define LIS3MDL_OUT_YH              (0x2B)
#define LIS3MDL_OUT_ZL              (0x2C)
#define LIS3MDL_OUT_ZH              (0x2D)

#define LIS3MDL_DATA_BASE			LIS3MDL_OUT_XL

/* Temp sensor */
#define LIS3MDL_TEMP_L              (0x2E)
#define LIS3MDL_TEMP_H              (0x2F)

/* CTRL_REG1 Settings */
#define LIS3MDL_RG1_TEMP_EN         (0x08)
#define LIS3MDL_RG1_OMXY_LP         (0x00)
#define LIS3MDL_RG1_OMXY_MP         (0x20)
#define LIS3MDL_RG1_OMXY_HP         (0x40)
#define LIS3MDL_RG1_OMXY_UHP        (0x60)
#define LIS3MDL_RG1_ODR_625mHz      (0x00)
#define LIS3MDL_RG1_ODR_1250mHz     (0x04)
#define LIS3MDL_RG1_ODR_2500mHz     (0x08)
#define LIS3MDL_RG1_ODR_5Hz         (0x0C)
#define LIS3MDL_RG1_ODR_10Hz        (0x10)
#define LIS3MDL_RG1_ODR_20Hz        (0x14)
#define LIS3MDL_RG1_ODR_40Hz        (0x18)
#define LIS3MDL_RG1_ODR_80Hz        (0x1C)
#define LIS3MDL_RG1_FASTODR         (0x02)
#define LIS3MDL_RG1_SELFTEST_EN     (0x01)

/* CTRL_REG2 Settings */
#define LIS3MDL_RG2_RANGE_400uT     (0x00)
#define LIS3MDL_RG2_RANGE_800uT     (0x20)
#define LIS3MDL_RG2_RANGE_1200uT    (0x40)
#define LIS3MDL_RG2_RANGE_1600uT    (0x60)
#define LIS3MDL_RG2_REBOOT          (0x08)
#define LIS3MDL_RG2_SOFTRST         (0x04)

/* CTRL_REG3 Settings */
#define LIS3MDL_RG3_LOWPWR_OFF      (0x00)
#define LIS3MDL_RG3_LOWPWR_ON       (0x20)
#define LIS3MDL_RG3_SIM_4WR         (0x00)  /* 4 wire spi */
#define LIS3MDL_RG3_SIM_3WR         (0x04)
#define LIS3MDL_RG3_MD_CON          (0x00)  /* continuous data conversion */
#define LIS3MDL_RG3_MD_SINGLE       (0x01)  /* single data conversion */
#define LIS3MDL_RG3_MD_PWRDWN       (0x03)

/* CTRL_REG4 Settings */
#define LIS3MDL_RG4_OMZ_LP          (0x00)
#define LIS3MDL_RG4_OMZ_MP          (0x04)
#define LIS3MDL_RG4_OMZ_HP          (0x08)
#define LIS3MDL_RG4_OMZ_UHP         (0x0C)
#define LIS3MDL_RG4_LITTLE_EN       (0x02)
#define LIS3MDL_RG4_BIG_EN          (0x00)

/* CTRL_REG5 Settings */
#define LIS3MDL_RG5_FASTREAD_EN     (0x80)
#define LIS3MDL_RG5_BDU_EN          (0x40)  /* Block data update */

#define LIS3MDL_GENOM_LP            (0x1)
#define LIS3MDL_GENOM_MP            (0x1 << 1)
#define LIS3MDL_GENOM_HP            (0x1 << 2)
#define LIS3MDL_GENOM_UHP           (0x1 << 3)

#define LIS3MDL_CTRL_REG3_SETTING ( LIS3MDL_RG3_LOWPWR_OFF | LIS3MDL_RG3_SIM_4WR | LIS3MDL_RG3_MD_SINGLE )

BMI160_RETURN_FUNCTION_TYPE bmi160_lis3mdl_init(void);

BMI160_RETURN_FUNCTION_TYPE bmi160_lis3mdl_check_whoami(void);

BMI160_RETURN_FUNCTION_TYPE bmi160_lis3mdl_start_collection(void);

BMI160_RETURN_FUNCTION_TYPE bmi160_lis3mdl_set_range(u8 sensor_range);

BMI160_RETURN_FUNCTION_TYPE bmi160_lis3mdl_set_rate(u8 config_rate);

BMI160_RETURN_FUNCTION_TYPE bmi160_lis3mdl_set_operating_mode(u8 gen_op_mode);

BMI160_RETURN_FUNCTION_TYPE bmi160_lis3mdl_read_mag_xyz(struct bmi160_mag_t *mag);

#endif /* BMI160_DRV_BMI160_LIS3MDL_H_ */
