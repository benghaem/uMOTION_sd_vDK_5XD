/*
 * bmi160_lis3mdl.c
 *
 *  Created on: Dec 2, 2016
 *      Author: Ben
 */
#include <bmi160_drv/bmi160_lis3mdl.h>

/* Get struct from main sensor driver */
extern struct bmi160_t *p_bmi160;

BMI160_RETURN_FUNCTION_TYPE bmi160_lis3mdl_init(void){

	BMI160_RETURN_FUNCTION_TYPE com_rslt = BMI160_INIT_VALUE;
	u8 v_data_u8 = BMI160_INIT_VALUE;
	u8 v_accel_power_mode_status = BMI160_INIT_VALUE;

	/* As per bmi160_bmm150_mag_interface init */

	com_rslt = bmi160_get_accel_power_mode_stat(
		&v_accel_power_mode_status);
	/* Accel operation mode to normal*/
	if (v_accel_power_mode_status != BMI160_ACCEL_NORMAL_MODE) {
		com_rslt += bmi160_set_command_register(ACCEL_MODE_NORMAL);
		p_bmi160->delay_msec(BMI160_GEN_READ_WRITE_DELAY);
	}

	/* --- */

	/* Enable mag interface */
	com_rslt += bmi160_set_mag_interface_normal();
	p_bmi160->delay_msec(BMI160_GEN_READ_WRITE_DELAY);

	/* Write LIS3MDL I2C address */
	com_rslt += bmi160_set_i2c_device_addr(LIS3MDL_I2C_ADDR);
	p_bmi160->delay_msec(BMI160_GEN_READ_WRITE_DELAY);

	/* enable the Mag interface to manual mode*/
	com_rslt += bmi160_set_mag_manual_enable(BMI160_MANUAL_ENABLE);
	p_bmi160->delay_msec(BMI160_GEN_READ_WRITE_DELAY);
	bmi160_get_mag_manual_enable(&v_data_u8);

	/*Enable the MAG interface */
	com_rslt += bmi160_set_if_mode(BMI160_ENABLE_MAG_IF_MODE);
	p_bmi160->delay_msec(BMI160_GEN_READ_WRITE_DELAY);
	bmi160_get_if_mode(&v_data_u8);

	/* Check the whoami */
	com_rslt += bmi160_lis3mdl_check_whoami();
	p_bmi160->delay_msec(BMI160_GEN_READ_WRITE_DELAY);

	/* Configure the sensor range */
	com_rslt += bmi160_lis3mdl_set_range(16);
	p_bmi160->delay_msec(BMI160_GEN_READ_WRITE_DELAY);

	/* Set the power mode */
	com_rslt += bmi160_lis3mdl_set_operating_mode(LIS3MDL_GENOM_HP);
	p_bmi160->delay_msec(BMI160_GEN_READ_WRITE_DELAY);

	/* Configure the collection rate */
	com_rslt += bmi160_lis3mdl_set_rate(LIS3MDL_RG1_ODR_80Hz);
	p_bmi160->delay_msec(BMI160_GEN_READ_WRITE_DELAY);

	/* Start data collection on the mag */
	com_rslt += bmi160_lis3mdl_start_collection();
	p_bmi160->delay_msec(BMI160_GEN_READ_WRITE_DELAY);

	/* Set the IMU mag collection rate */
	com_rslt += bmi160_set_mag_output_data_rate(BMI160_MAG_OUTPUT_DATA_RATE_100HZ)
	p_bmi160->delay_msec(BMI160_GEN_READ_WRITE_DELAY);

	/* Set address to read from */
	com_rslt += bmi160_set_mag_read_addr(LIS3MDL_DATA_BASE);
	p_bmi160->delay_msec(BMI160_GEN_READ_WRITE_DELAY);

	/* Set burst length */
	com_rslt += bmi160_set_mag_burst(6);
	p_bmi160->delay_msec(BMI160_GEN_READ_WRITE_DELAY);

	/* Enable MAG Auto Mode */
	com_rslt += bmi160_set_mag_manual_enable(BMI160_MANUAL_DISABLE);
	p_bmi160->delay_msec(BMI160_GEN_READ_WRITE_DELAY);
	bmi160_get_mag_manual_enable(&v_data_u8);
	p_bmi160->delay_msec(BMI160_GEN_READ_WRITE_DELAY);

	return com_rslt;
}

BMI160_RETURN_FUNCTION_TYPE bmi160_lis3mdl_check_whoami(void){
	BMI160_RETURN_FUNCTION_TYPE com_rslt = BMI160_INIT_VALUE;

	com_rslt += bmi160_set_mag_read_addr(LIS3MDL_WHOAMI_REG);
	p_bmi160->delay_msec(BMI160_GEN_READ_WRITE_DELAY);
	com_rslt += bmi160_read_reg(BMI160_MAG_DATA_READ_REG,
	&v_data_u8, BMI160_GEN_READ_WRITE_DATA_LENGTH);
	p_bmi160->delay_msec(BMI160_GEN_READ_WRITE_DELAY);

	if (v_data_u8 != LIS3MDL_WHOAMI){
		com_rslt += ERROR;
	}

	return com_rslt;
}

BMI160_RETURN_FUNCTION_TYPE bmi160_lis3mdl_start_collection(void){
	BMI160_RETURN_FUNCTION_TYPE com_rslt = BMI160_INIT_VALUE;
	u8 v_data_u8 = BMI160_INIT_VALUE;

	/* Enable data conversion by setting values in CTRL_REG3 */
	com_rslt += bmi160_set_mag_write_data(LIS3MDL_CTRL_REG3_SETTING);
	/* Write to CTRL_REG3 */
	com_rslt += bmi160_set_mag_write_addr(LIS3MDL_CTRL_REG3);

	return com_rslt;
}

/*
 * @breif Set the range of the sensor
 * 
 * @param sensor_range
 * either 4,8,12,16
 * 4 by default
 *
 */

BMI160_RETURN_FUNCTION_TYPE bmi160_lis3mdl_set_range(u8 sensor_range){
	BMI160_RETURN_FUNCTION_TYPE com_rslt = BMI160_INIT_VALUE;

	/* no reason to read the reg first because the only other flags here are for
	   reboot and reset */
	u8 reg_base = 0x00;
	switch(sensor_range){
		case 4:
			reg_base = reg_base | LIS3MDL_RG2_RANGE_400uT;
			break;
		case 8:
			reg_base = reg_base | LIS3MDL_RG2_RANGE_800uT;
			break;
		case 12:
			reg_base = reg_base | LIS3MDL_RG2_RANGE_1200uT;
			break;
		case 16:
			reg_base = reg_base | LIS3MDL_RG2_RANGE_1600uT;
			break;
		default:
			/* do nothing --> already set for 4gauss*/
			break;
	}


	/* Change range by setting values in CTRL_REG2 */
	com_rslt += bmi160_set_mag_write_data(reg_base);
	/* Write to CTRL_REG3 */
	com_rslt += bmi160_set_mag_write_addr(LIS3MDL_CTRL_REG2);

	return com_rslt;
}



BMI160_RETURN_FUNCTION_TYPE bmi160_lis3mdl_set_operating_mode(u8 gen_op_mode){
	BMI160_RETURN_FUNCTION_TYPE com_rslt = BMI160_INIT_VALUE;

	/* Need REG1 and REG4 */
	u8 reg1 = 0;	
	u8 reg4 = 0;
	com_rslt += bmi160_set_mag_read_addr(LIS3MDL_CTRL_REG1);
	p_bmi160->delay_msec(BMI160_GEN_READ_WRITE_DELAY);

	com_rslt += bmi160_read_reg(BMI160_MAG_DATA_READ_REG, &reg1, BMI160_GEN_READ_WRITE_DATA_LENGTH);
	p_bmi160->delay_msec(BMI160_GEN_READ_WRITE_DELAY);

	com_rslt += bmi160_set_mag_read_addr(LIS3MDL_CTRL_REG4);
	p_bmi160->delay_msec(BMI160_GEN_READ_WRITE_DELAY);

	com_rslt += bmi160_read_reg(BMI160_MAG_DATA_READ_REG, &reg4, BMI160_GEN_READ_WRITE_DATA_LENGTH);
	p_bmi160->delay_msec(BMI160_GEN_READ_WRITE_DELAY);

	/* clear opmode bits */
	reg1 &= ~(0x60);
	reg4 &= ~(0x0C);

	switch(gen_op_mode){
		case LIS3MDL_GENOM_UHP:
			reg1 |= LIS3MDL_RG1_OMXY_UHP;
			reg4 |= LIS3MDL_REG4_OMZ_UHP;
			break;
		case LIS3MDL_GENOM_HP:
			reg1 |= LIS3MDL_RG1_OMXY_HP;
			reg4 |= LIS3MDL_REG4_OMZ_HP;
			break;
		case LIS3MDL_GENOM_MP:
			reg1 |= LIS3MDL_RG1_OMXY_MP;
			reg4 |= LIS3MDL_REG4_OMZ_MP;
			break;
		case LIS3MDL_GENOM_LP:
		default:
			reg1 |= LIS3MDL_RG1_OMXY_LP;
			reg4 |= LIS3MDL_REG4_OMZ_LP;
			break;
	}

	com_rslt += bmi160_set_mag_write_data(reg1);
	com_rslt += bmi160_set_mag_write_addr(LIS3MDL_CTRL_REG1);
	p_bmi160->delay_msec(BMI160_GEN_READ_WRITE_DELAY);

	com_rslt += bmi160_set_mag_write_data(reg4);
	com_rslt += bmi160_set_mag_write_addr(LIS3MDL_CTRL_REG4);
	p_bmi160->delay_msec(BMI160_GEN_READ_WRITE_DELAY);

	return com_rslt;
}

/*
 * @breif Set the range of the sensor
 * 
 * @param config_rate
 *
 */

BMI160_RETURN_FUNCTION_TYPE bmi160_lis3mdl_set_rate(u8 config_rate){
	BMI160_RETURN_FUNCTION_TYPE com_rslt = BMI160_INIT_VALUE;

	u8 reg_base;

	com_rslt += bmi160_set_mag_read_addr(LIS3MDL_CTRL_REG1);
	p_bmi160->delay_msec(BMI160_GEN_READ_WRITE_DELAY);

	com_rslt += bmi160_read_reg(BMI160_MAG_DATA_READ_REG, &reg_base, BMI160_GEN_READ_WRITE_DATA_LENGTH);
	p_bmi160->delay_msec(BMI160_GEN_READ_WRITE_DELAY);

	/* clear rate bits */

	reg_base &= ~(0x1C);
	reg_base |= config_rate;

	/* Change rate by setting value in CTRL_REG1 */
	com_rslt += bmi160_set_mag_write_data(reg_base);
	/* Write to CTRL_REG1 */
	com_rslt += bmi160_set_mag_write_addr(LIS3MDL_CTRL_REG1);

	return com_rslt;
}

BMI160_RETURN_FUNCTION_TYPE bmi160_lis3mdl_read_mag_xyz(struct bmi160_mag_t *mag)
{
	BMI160_RETURN_FUNCTION_TYPE com_rslt = BMI160_INIT_VALUE;

	/*styled after the main driver */
	u8 v_data_u8[BMI160_MAG_XYZ_DATA_SIZE] = {
	BMI160_INIT_VALUE, BMI160_INIT_VALUE,
	BMI160_INIT_VALUE, BMI160_INIT_VALUE,
	BMI160_INIT_VALUE, BMI160_INIT_VALUE};

	if (p_bmi160 == BMI160_NULL){
		return E_BMI160_NULL_PTR;
	} else {
		com_rslt = p_bmi160->BMI160_BUS_READ_FUNC(
			p_bmi160->dev_addr,
			BMI160_USER_DATA_MAG_X_LSB__REG, v_data_u8, BMI160_MAG_XYZ_DATA_LENGTH);
			v_data_8
	}

}