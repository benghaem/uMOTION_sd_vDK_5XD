/*
 * Copyright (c) 2015-2016, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== empty.c ========
 */



/* stdlib and such */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

/* XDCtools Header files */
#include <xdc/std.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/Diags.h>
#include <xdc/runtime/Log.h>
#include <xdc/runtime/Error.h>

/* BIOS Header files */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Mailbox.h>
#include <ti/sysbios/knl/Semaphore.h>

/* TI-RTOS Header files */
#include <ti/drivers/I2C.h>
#include <ti/drivers/PIN.h>
#include <ti/drivers/SPI.h>
#include <ti/drivers/SDSPI.h>
#include <time_util.h>

/* Custom SDSPI implementation */
#include "SDSPICC2650.h"

/* Board Header files */
#include "Board.h"

/* INIT CFG header */
#include "init_cfg.h"

/* Sensor driver files */
#include "bmi160_drv/bmi160.h"
#include "bmi160_drv/bmi160_support.h"
#include "bmi160_drv/bmi160_lis3mdl.h"

/* Utilities */
#include "data_util.h"
#include "fs_util.h"

/* Fat FS */
#include <ti/mw/fatfs/ff.h>


/* Settings */
#include "uMOTION_cfg_vars.h"

/* Task settings */
#define TASKSTACKSIZE_LARGE   2500
#define TASKSTACKSIZE_SMALL   700
/* FAT FS settings */
#define MOUNT_NUM 0


/* Task Structures */

Task_Struct BMI_sensor_task_struct;
Task_Struct sd_control_task_struct;

Char BMI_sensor_task_stack[TASKSTACKSIZE_LARGE];
Char sd_control_task_stack[TASKSTACKSIZE_LARGE];

/* General Config */
struct ICFG_Config uMOTION_Config;

/* Clock configuration for sensors */
Clock_Params sensor_clk_params;
Error_Block sensor_eb;
Clock_Handle sensor_clk_handle;

/* Mailbox */
Mailbox_Handle sensor_mailbox;

/* BMI_init semaphor*/
Semaphore_Handle sd_init_sem;



/* 
 * ======== Update sensors =======
 * Function to update sensor values 
 */ 

Void update_sensors(UArg arg0){
    
    struct bmi160_gyro_t samp_gyroxyz;
    struct bmi160_accel_t samp_accelxyz;
    struct bmi160_mag_t samp_magxyz;

    uint8_t samp_buffer[MAX_PACK_SIZE];
    uint8_t flags = 0x00;

    if (uMOTION_Config.enable_acc){
        bmi160_read_accel_xyz(&samp_accelxyz);
        flags |= PACK_ACC;
    }
    if (uMOTION_Config.enable_gyro){
        bmi160_read_gyro_xyz(&samp_gyroxyz);
        flags |= PACK_GYRO;
    }

    //Get sampling tick here
    uint16_t tick = (uint16_t)Clock_getTicks();

    //Pack everything into an array based on enabled sensors
    pack_data(flags, (uint16_t *)samp_buffer, &tick, &samp_accelxyz, &samp_gyroxyz, &samp_magxyz);

    /* Post to the mailbox */
    if (Mailbox_post(sensor_mailbox, samp_buffer, BIOS_NO_WAIT) == FALSE){
        Log_print1(Diags_USER1, "Mailbox: Overflow", 0);
    };
    
}

/*
 * ======== bmi160 =======
 * Enable bmi160
 */
Void BMI_sensor_task(UArg arg0){

    /* 
     * TODO: Should include configuation for which sensor modules
     * turn on here. Using uMOTION_Config.enable_acc / uMOTION_Config.enable_gyro
     */

    //Wait for sd to ready up
    Semaphore_pend(sd_init_sem, BIOS_WAIT_FOREVER);


    /* Init BMI160 */
    if (!bmi160_initialize_sensor(uMOTION_Config.acc_range, uMOTION_Config.gyro_range)){
        Log_print1(Diags_USER1, "BMI: INIT Success", 0);
    } else {
        Log_print1(Diags_USER1, "BMI: INIT Failed", 0);
        System_abort("Init bmi falied/n");
    }

    /* Init Mag Sensor */
    if (!bmi160_lis3mdl_init()){
        Log_print1(Diags_USER1, "BMI: MAG INIT Success", 0);
    } else {
        Log_print1(Diags_USER1, "BMI: MAG INIT Failed", 0);
        System_abort("Init BMI MAG falied/n");
    }

    
    struct bmi160_gyro_t samp_gyroxyz;
    struct bmi160_accel_t samp_accelxyz;
    struct bmi160_mag_t samp_magxyz;

   // struct bmi160_fifo_data_header_t samp_fifo;

    uint8_t samp_buffer[MAX_PACK_SIZE];
    uint8_t flags = 0x00;

    uint32_t last_tick = Clock_getTicks();


    while(1){

        //Get sampling tick here
        uint32_t full_tick= Clock_getTicks();

        if (full_tick - last_tick < uMOTION_Config.samp_period){
            continue;
        }
        last_tick = full_tick;
        if (uMOTION_Config.enable_acc){
            bmi160_read_accel_xyz(&samp_accelxyz);
            flags |= PACK_ACC;
        }
       // Task_sleep(10);
        if (uMOTION_Config.enable_gyro){
            bmi160_read_gyro_xyz(&samp_gyroxyz);
            flags |= PACK_GYRO;
        }

        //bmi160_read_fifo_header_data_user_defined_length(512, 0, &samp_fifo);

        uint16_t tick = (uint16_t)full_tick;

        //Pack everything into an array based on enabled sensors
        pack_data(flags, (uint16_t *)samp_buffer, &tick, &samp_accelxyz, &samp_gyroxyz, &samp_magxyz);

        /* Post to the mailbox */
        Mailbox_post(sensor_mailbox, samp_buffer, BIOS_WAIT_FOREVER);

    }
}



Void sd_control_task(UArg arg0){
	SDSPI_Handle sdspiHandle;
    SDSPI_Params sdspiParams;

    /* Variables for the CIO functions */
    FILE *data, *cfg;

//    System_printf("Starting SDSPI test\n");
//	System_flush();

    /* Wait for SD card to fully power up */
//	Task_sleep(1000);

    /* Mount and register the SD Card */
    SDSPI_Params_init(&sdspiParams);
    sdspiHandle = SDSPI_open(Board_SDSPI0, MOUNT_NUM, &sdspiParams);
    if (sdspiHandle == NULL) {
        System_abort("Error starting the SD card\n");
    } else {
        System_printf("Drive %u is mounted\n", MOUNT_NUM);
        System_flush();
    }

    /* First check to see if we have a config file */

    cfg = fopen("fat:0:init.cfg", "r");
    if (cfg != NULL){
        /* Read values from the config file */
        char line[30];
        int val;
        while (fgets(line, 30, cfg) != NULL){
            /* Check for lines that start with comments and ignore them */
            if (!strncmp("#", line, 1)){
                continue;
            }
            /* Check for other keys */
            if (!strncmp("ACC_EN", line, 6)){
                val = atoi(line+6+1);
                if (val == 0 || val == 1){
                    uMOTION_Config.enable_acc = val;
                    System_printf("Enable ACC set to: %d\n", val);
                    System_flush();
                }
            } else if (!strncmp("GYRO_EN", line, 7)){
                val = atoi(line+7+1);
                if (val == 0 || val == 1){
                    uMOTION_Config.enable_gyro = val;
                    System_printf("Enable Gyro set to: %d\n", val);
                    System_flush();
                }
            } else if (!strncmp("SAMP_PERIOD", line, 11)){
                val = atoi(line+11+1);
                if (val >= ICFG_SAMPLING_PERIOD_MIN || val <= ICFG_SAMPLING_PERIOD_MAX ){
                    uMOTION_Config.samp_period = val;
                    System_printf("Sensor sampling period set to: %dms\n", val);
                    System_flush();
                }
            } else if (!strncmp("SAMP_GCOUNT", line, 11)){
                val = atoi(line+11+1);
                if (val >= ICFG_SAMPLE_GROUPS_MIN ){
                    uMOTION_Config.samp_group_count = val;
                    System_printf("Sensor sample group count set to: %d\n", val);
                    System_flush();
                }
            } else if (!strncmp("GRYO_RANGE", line, 10)){
                val = atoi(line+10+1);
                switch (val){
                    case 125:
                        uMOTION_Config.gyro_range = BMI160_GYRO_RANGE_125_DEG_SEC;
                        break;
                    case 250:
                        uMOTION_Config.gyro_range = BMI160_GYRO_RANGE_250_DEG_SEC;
                        break;
                    case 500:
                        uMOTION_Config.gyro_range = BMI160_GYRO_RANGE_500_DEG_SEC;
                        break;
                    case 1000:
                        uMOTION_Config.gyro_range = BMI160_GYRO_RANGE_1000_DEG_SEC;
                        break;
                    case 2000:
                    default:
                        uMOTION_Config.gyro_range = BMI160_GYRO_RANGE_2000_DEG_SEC;
                }
            } else if (!strncmp("ACC_RANGE", line, 9)){
                val = atoi(line+9+1);
                switch (val){
                    case 2:
                        uMOTION_Config.acc_range = BMI160_ACCEL_RANGE_2G;
                        break;
                    case 4:
                        uMOTION_Config.acc_range = BMI160_ACCEL_RANGE_4G;
                        break;
                    case 8:
                        uMOTION_Config.acc_range = BMI160_ACCEL_RANGE_8G;
                        break;
                    case 16:
                    default:
                        uMOTION_Config.acc_range = BMI160_ACCEL_RANGE_16G;
                }
            }
        }
    } else {
        System_printf("No config file found. using defaults\n");
        System_printf("Creating blank config to initialize SD card\n");
        System_flush();
        cfg = fopen("fat:0:init.cfg", "w+");
        if (cfg != NULL){
            /*
             * Write out example config so that we can confirm that the SD
             * card is active
             */

            fwrite("#KEY:VALUE",sizeof(char),10,cfg);
            fflush(cfg);

        } else {
            System_abort("Unable to initialize SD card\n");
        }
    }

    uint8_t mailbox_size = BASE_PACK_SIZE; 

    if (uMOTION_Config.enable_acc){
        mailbox_size += ACC_PACK_SIZE;
    }

    if (uMOTION_Config.enable_gyro){
        mailbox_size += GYRO_PACK_SIZE;
    }

    /* Create a new mailbox with buffer sizes that fit enabled sensors */
    sensor_mailbox = Mailbox_create(mailbox_size, MAILBOX_BUFFER_COUNT, NULL, NULL);

    /* Close file when done */
    int cfg_close_ret = fclose(cfg);

    //Search for previous data_files
    int previous_id = last_data_file_id();

    //filenames are capped to 13 characters + 11 for the lead in
    char data_filename[24];
    snprintf(data_filename, 24,"fat:0:data/d_%d.bin",previous_id+1);

    System_printf("Previous file id: %d\n",previous_id);
    System_printf("Creating new data file: %s\n",data_filename);
    System_flush();


    //Make data dir if it does not already exist
    f_mkdir("data");
    //Open a data file here so that we have somewhere to put data
    data = fopen(data_filename,"w+");
    if (data == NULL){
        System_abort("Unable to open data file for writing\n");
    }

    /* Pend for completion of sd init tasks */
    Semaphore_post(sd_init_sem);

    /* When other init tasks have completed then we wait for our mailbox */
    int running = uMOTION_Config.samp_group_count * 20;
    int mbx_buffer_cnt = 0;
    uint8_t mailbox_out[WRITE_BUFFER_COUNT][BASE_PACK_SIZE+ACC_PACK_SIZE+GYRO_PACK_SIZE] = {0};
    while (running > 0){
        /*Mailbox has given us a new message-->copy it to the buffer */

        Mailbox_pend(sensor_mailbox, mailbox_out[mbx_buffer_cnt], BIOS_WAIT_FOREVER);
        mbx_buffer_cnt++;
        if (mbx_buffer_cnt == WRITE_BUFFER_COUNT){
            mbx_buffer_cnt = 0;
            fwrite("STAR",sizeof(char),4,data);
            fwrite(mailbox_out, sizeof(uint8_t), WRITE_BUFFER_COUNT * (BASE_PACK_SIZE+ACC_PACK_SIZE+GYRO_PACK_SIZE), data);
            fwrite("TEND",sizeof(char),4,data);

            running -= WRITE_BUFFER_COUNT;

        }

    }

    fwrite("EEFF",sizeof(char),4,data);

    /* Check for errors on file close */
    if(fclose(data)){
        System_printf("Failed to close data file\n");
        System_flush();
    }

    /* Stopping the SDCard */
    SDSPI_close(sdspiHandle);

    asm(" NOP");

    System_printf("Data collection complete\n");
    System_flush();


}

/*
 *  ======== main ========
 */
int main(void)
{
    Task_Params sd_control_task_params;
    Task_Params BMI_sensor_task_params;

    System_printf("\nuMOTION INERTIA 2016\nStarting up board...\n");
    System_flush();

    /* Call board init functions */
    Board_initGeneral();
    Board_initI2C();
    Board_initSPI();
    Board_initSDSPI();


    /* Set general defaults */
    uMOTION_Config.enable_acc = ICFG_ENABLE_ACC;
    uMOTION_Config.enable_gyro = ICFG_ENABLE_GYRO;
    uMOTION_Config.samp_period  = ICFG_SAMPLING_PERIOD;
    uMOTION_Config.samp_group_count = ICFG_SAMPLE_GROUPS;
    uMOTION_Config.gyro_range = ICFG_GYRO_RANGE;
    uMOTION_Config.acc_range = ICFG_ACC_RANGE;


    /* Initialize SD control task */
    /* This task will check for config files and init the SD card */
    /* Runs at low priority and will yield when ready so that other init may continue */
    Task_Params_init(&sd_control_task_params);
    sd_control_task_params.stackSize = TASKSTACKSIZE_LARGE;
    sd_control_task_params.stack = &sd_control_task_stack;
    sd_control_task_params.arg0 = 0x00;
    sd_control_task_params.priority = 1;
    Task_construct(&sd_control_task_struct, (Task_FuncPtr)sd_control_task, &sd_control_task_params, NULL);

    /* Initialize BMI160 sensor task */
    /* This task starts the motion sensor and starts the data recording functions*/
    /* High priority to run after the SD control task yields */
    Task_Params_init(&BMI_sensor_task_params);
    BMI_sensor_task_params.stackSize = TASKSTACKSIZE_LARGE;
    BMI_sensor_task_params.stack = &BMI_sensor_task_stack;
    BMI_sensor_task_params.arg0 = 0x00;
    BMI_sensor_task_params.priority = 2;
    Task_construct(&BMI_sensor_task_struct, (Task_FuncPtr)BMI_sensor_task, &BMI_sensor_task_params, NULL);

    sd_init_sem = Semaphore_create(0, NULL, NULL);


    /* Start BIOS */
    BIOS_start();

    return (0);
}
