/*
 * fs_util.c
 *
 *  Created on: Mar 10, 2017
 *      Author: bg5ng
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "fs_util.h"
#include <ti/mw/fatfs/ff.h>

int last_data_file_id(void){
    FRESULT fr;  /* Return value from FatFs*/
    DIR dj;
    FILINFO fno; /* File info obj */

    // look in data folder for data_*.bin files
    fr = f_findfirst(&dj, &fno, "data", "d_*.bin");

    //if an item is not found we will return 0
    int largest_id = 0;
    int cmp_id = 0;

    //while ok and the first char of the filename is not null
    while(fr == FR_OK && fno.fname[0]) {
        //Filenames look like D_# thus we want to start at index 2 for atoi
        cmp_id = atoi(fno.fname+2);
        if (cmp_id > largest_id){
            largest_id = cmp_id;
        }
        fr = f_findnext(&dj, &fno);
    }
    return largest_id;

}
