#ifndef _TIME_UTIL_H_
#define _TIME_UTIL_H_

#include <stdint.h>

uint32_t get_tick();

#endif /* _TIME_UTIL_H_ */
