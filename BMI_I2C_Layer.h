#ifndef __BMI_I2C_LAYER__
#define __BMI_I2C_LAYER__

#include "bmi160_drv/bmi160.h"

s8 I2C_BMI_bus_write(u8 device_addr, u8 register_addr,
								u8* data, u8 length);

s8 I2C_BMI_bus_read(u8 device_addr, u8 register_addr,
								u8* data, u8 length);

#endif
